This document contains an overview of the various post-processing scripts used in the AVLab at University of California Santa Barbara Library Special Resource Collections.
Please see AVLab Utility Software List & Installation Instructions (on the wiki) for more info.

# Intro
The UCSB Special Resource Collections Audio Visual lab uses a microservices structure for our Python based AV processing scripts.  The microservices pattern splits programs and processing steps into small, discreet, modular scripts, which can call each other to fit particular uses and workflows.

To run these scripts, open a terminal (on Mac) or a Command prompt (on Windows) and type `python [path of python file] [arguments]`

Each script can also be run with a ` -h` flag to output some help text.

# Configuration
The scripts rely on a configuration file named `microservices-config.ini`. Due to security concerns the current version of the config file is no longer tracked in our git version control. The version included in the git repo can serve as template/example only.  The values in the config file are not valid for production use.

For UCSB users, the "canonical" config file can be found at `S:/avlab/microservices/microservices-config.ini`

For ease of updating, the python installations used to run microservices scripts is stored on the network drive.

For Windows users this is currently at:
`S:\avlab\win_venv\Script\python.exe`

For Mac users this is currently at: 
`/Volumes/special/DeptShare/special/avlab/venv/bin/python3.11` 

| Script name                  | start method                                                          | used in processing       | Operating system typical use | Workstation  for typical use | use mode |
|------------------------------|-----------------------------------------------------------------------|--------------------------|------------------------------|------------------------------|----------|
| `avlab-audio.py            ` | Shell/Terminal/Cmd window                                             | magnetic tape recordings | Windows                      | Cassettes / Open Reel        | direct   |
| `rip-optical-disc.py       ` | Shell/Terminal/Cmd window                                             | audio                    | windows                      | ?                            | direct   |
| `avlab-cylinders.py        ` | Shell/Terminal/Cmd window                                                                    | cylinder recordings      | Windows                      | Cylinders                    | direct        |
| `avlab-video.py            ` | Shell/Terminal/Cmd Window                                                                     | video recordings         | Windows                      | any                           | direct        |
| `embedimg_photoInMp3.py    ` | Shell/Terminal/Cmd window                                             | cylinders                | ?                            | ?                            | direct   |
| `makeDerivativesForWeb.py  ` | Shell/Terminal/Cmd window                                             | cylinder recordings      | any                          | Mac                          | direct   |
| `makebroadcast.py          ` | Shell/Terminal/Cmd window                                             | audio and video          | windows                      | any                          | direct   |
| `makedip.py                ` | Shell/Terminal/Cmd window                                             | audio and video          | windows                      | any                          | direct   |
|  <br> |  |  |  |  |  | 
| `makereverse.py            ` | as a Sub-process from another script                                  | audio                    | windows                      | Cassettes / Open Reel        | indirect |
| `makesip.py                ` | as a Sub-process from another script                                  |                          |                              |                              | indirect |
| `makestartobject.py        ` | included in another Python script as an _import_ and as a sub-process | audio and photo                        | windows                      | all                          | indirect |
| `makeqctoolsreport.py      ` | as a Sub-process from another script                                  | video                    | windows                      | any                          | indirect  |
| `mtd.py                    ` | included in another python script as an _import_                      |                          |                              |                              | indirect |
| `phi_audio.py              ` | Filemaker                                                             | 78rpm discs              | windows                      | 78rpm disc                   | indirect |
| `phi_discimg-capture.py    ` | Filmaker                                                              | photos                   | mac                          | photo workstation            | indirect |
| `phi_discimg-out.py        ` | Filemaker                                                             | photos                   | Mac                          | photo workstation            | indirect |
| `util.py                   ` | included in another Python script as an _import_                      | all                      | all                          | all                          | indirect |
| `phi_discimg-capture-fm.sh ` | Filemaker                                                             | photos                   | Mac                          | photo workstation            | indirect |
| `phi_discimg-out-fm.sh     ` | Filemaker                                                             | photos                   | Mac                          | photo workstation            | indirect | 
| `avlab-discs.py            ` | Filemaker                                                             | 78rpm disc recordings    | Windows                      | 78rpm disc                   | indirect |
| `capture-image.py          ` | Filemaker                                                             | disc photos              | Mac                          | photo workstation            | indirect |
| `config.py                 ` | included in another Python script as an _import_                      | any / all                | all                          | all                          | indirect |
| `ff.py                     ` | included in another Python script as an _import_                      | any / all                | all                          | all                          | indirect |
| `hashmove.py               ` | included in another Python script as an _import_                      | all                      | all                          | all                          | indirect |
| `logger.py                 ` | included in another Python script as an _import_                      | all                      | all                          | all                          | indirect |
| `makeaccess.py             ` | as a Sub-process from another script                                  | audio                    | windows                      | Cassettes / Open Reel        | indirect |
|`fixt.py                   ` | n/a                                                                   |                          |                              |                              |          |
|`stream2filemaker.py       ` | ?                                                                     | ?                        | ?                            | ?                            | ?        |



# make _something_ scripts

- `makeDerivativesForWeb.py`
- `makeaccess.py`
- `makebarcodes.py`
- `makebroadcast.py`
- `makedip.py`
- `makeqctoolsreport.py`
- `makereverse.py`
- `makesip.py`
- `makestartobject.py`

The _make-scripts_ are the atomic units of our microservices. They typically process a single audio or video file.  These are intended to be run directly by an operator/user in the terminal in an interactive shell.


#### `makeacces.py`
Processes an input file, generally a broadcast master, transcodes to 320kbps mp3. Embeds ID3 tags if present (either in source file or in sidecar txt file). Embeds png image of "Cover Art" if png or tif present in source directory.

#### `makebroadcast.py`
Takes an input file, generally an archival master or raw-broadcast capture, inserts 2s fades, drops bitrate to 44.1k/16bit, embeds ID3 metadata if source txt file is present.

Has flags for fades (-ff), national jukebox names (-nj), stereo (-s), normalize (-n)

#### `makedip.py`

make a Dissemination Information Package [(DIP) *](https://www.iasa-web.org/tc04/formats-and-dissemination-information-packages-dip) file.

Takes an input string such as `a1234, cusb_col_a12_01_5678_00`, which is the name for a digitized object and an Aeon transaction number to which the resulting DIP will be linked. This script then transcodes from source objects if necessary, hashmoves them to a DIP directory, zips the DIP directory into a file suitable for upload to Aeon server.

Has flags for: 

- startObject `-so`
- transactionNumber `-tn`
- `--tape` for tape mode
- `-z` to make a Zip file

#### `makeqctoolsreport.py`
Takes an input video file, and outputs a compressed xml file that can be read by QCTools. It has to transcode it to a raw video format first so this script takes some time and processor space and is generally run Friday afternoon over a week of new captures, and runs into the weekend.

#### `makereverse.py`
Takes an input file and stream-copies reversed slices of 600 seconds, then concatenates the slices back together. ffmpeg's areverse loads a whole file into memory, so for the large files we deal with we need this workaround.

#### `makesip.py`
makesip takes an input folder path and uses our rules to create a Submission Information Package (SIP)

#### `makestartobject.py`
Takes a canonical name for an asset, e.g. a1234, cusb_col_a1234_01_5678_00, and returns a full path to an instance of that object. The instance is prioritized for transcoding in the following order: broadcast master, archival master, un-tagged archival master, access mp3.

#### `makevideoslices`.py
Slices preservation and access transfers of videos with more than 1 asset on the tape (eg. vm1234 also contains vm5678 and vm9101). Takes no arguments but you have to edit the in and out points in a list in the script, as well as corresponding vNums. Needs to be better, OpenCube editing interface is crappy and Premiere doesn't accept our preservation masters so.....

#### `makeyoutube.py`
Still in development, this script makes a youtube-ready video for a digitized 78rpm disc, based on metadata in DAHR

#### `makeDerivativesForWeb.py`

Walk a configured "rootDir" directory, select a source .WAV file preferring the "Broadcast" .WAV if available,
and create a audio level normalized MP3 file for any directory/recording that doesn't already have an MP3 file.
This script makes use of Werner Robitza's [ffmpeg-normalize](https://github.com/slhck/ffmpeg-normalize) tool to normalize the MP3 to [EBU R 128](https://en.wikipedia.org/wiki/EBU_R_128) standard.

Example invocation:  

```
python makeDerivativesForWeb.py
```


### library scripts

These scripts are basically collections of functions that are invoked by other scripts.

#### `config.py`
config parses the `microservices-config.ini` file and returns a dictionary object that is accessible via dot notation, 
e.g. `conf.cylinders.new_ingest` rather than `conf['cylinders']['new_ingest']`.  
conf is declared globally in every script, this module is imported as `rawconfig` and `conf = rawconfig.config()` actually makes the conf dictionary.

#### `ff.py`
ff does everything ffmpeg-related.  
principally, it uses the provided dictionary objects to construct strings that can be sent to ffmpeg.  
It also describes a framework ('ff.go') which runs the supplied ffmpeg string and returns true if successful, the error if unsuccessful

#### `logger.py`
logger is the handler for all logging functions. 
Generally, it makes logs for each script's full execution, linking sub-scripts to their parents via PIDs. logs are named "user-pid-timestamp.log"

#### `mtd.py`
mtd is short for "metadata" and it's the handler for all FileMaker and catalog access.  
It implements pyodbc and a SRU string to get metadata from external sources, or to insert metadata into FileMaker.

#### `util.py`
util is the handler for all utility functions.

Two noteworthy ones are: 
- `desktop()` - which returns the current user's desktop folder; 
- `dotdict(dict)` - which takes a regular dictionary and makes it accessible via dot notation, e.g. `conf.cylinders.new\_ingest` rather than `conf['cylinders']['new\_ingest']`

#### `avlab.py`
These processes deal with all of the audio objects created in the AVLab, not part of the Jukebox

#### `avlab-audio.py`

Post-processing for magnetic-tape materials. Gets metadata from Audio Originals FileMaker db

does the following:

- Deletes sidecar files that Wavelab generates
- makes a list of files to process based on FileMaker outputs (see staff wiki)
- Deletes silences of longer than 30s
- Embeds bext info, data chunk md5
- hashmoves it to the repo directory, greps the output of hashmove and, if successful, deletes raw capture and assocaited txt files

#### `avlab-cylinders.py`
Post-processing of cylinders and the creation of derivative files.   
Gets metadata from Cylinders FileMaker db.

#### `avlab-discs.py`
Post-processing for discs that are not part of National Jukebox process.  
Bundles makemp3 and makebroadcast with configurable filepaths.  
Used for post-processing of patron requests, mostly, saves files to a QC directory on R:/.  
Has dependencies for ffmpeg, ffprobe, bwfmetaedit

#### `avlab-video.py`
post-processing for archival master video files.  
bundles makeqctoolsreport; makes a PBCore2.0 compliant xml file of technical metadata.

#### `phi_discimg-capture.py`
This script is used exclusively during disc label imaging and is called by FileMaker.  
It is run in conjunction with NJ Workflow DB's "discimg-in" script.   
 For more info on the process of capturing disc label images, see Disc Imaging for National Jukebox  
It takes one argument for a barcode (which Filemaker provides)  

The processing steps are:

- do a string substitution for "ucsb" to "cusb
- corrects a long-standing programming error in FM
- using the filename provided, check that it doesn't already exist in the capture directory
- return an error if it does
- take a picture using gphoto2
- sort the capture dir by file creation date
- rename the most recently created file to the barcode/filename provided
- walk through the capture directory and check to see that no files exist with their raw-capture names
- this indicates that we missed scanning a barcode
- return an error if true

#### `phi_discimg-out.py`
This script is used to process an intermediate set of disc label digital images.  
 For more info, see Disc Imaging for National Jukebox  
It takes no arguments.  

The processing steps are:

- for each file in the intermediate directory
- call GraphicsMagick to crop, rotate, adjust ppi, and save as tif in a new folder
- here's what that command would look like if you typed it out for each file
- gm convert [full path to input file] -crop 3648x3648+920 -density 300x300 -rotate 180 [full path to output .tif]
- hashmove the raw-capture, dng intermediate, and broadcast tif to the qc directory/ purgatory

here's what that command would look like if you typed it out for each file

`python hashmove.py [full path to input file] [full path to qc directory + /discname]`



## Tips

### on Windows change your default `cmd.exe` directory to the repo directory

- Start -> search "regedit" -> double-click "regedit.exe"
- HKEY_CURRENT_USER -> Software -> Microsoft -> Command Processor
- right click on the big empty space and select "new key"
- type "Autorun" and hit enter
- right-click "Autorun" in the regedit window and select "Edit"
- type "cd/ d [path to repo directory]"
- By doing this, you are set to open the cmd window in the directory with all the scripts so you don't have to type their full paths

### hashmove

file movement for people wihtout rsync

**General Usage**

```
python hashmove.py [source file or directory full path] [destination parent directory full path] [flags]
```

**to move a file**

```
python hashmove.py C:/path/to/file.ext C:/path/to/parent/dir
```

**to move a directory**

```
python hashmove.py /home/path/to/dir/a /home/path/to/dir/b
```

**to copy a file**

```
python hashmove.py C:/path/to/file.ext C:/path/to/parent/dir -c
```

**log the transfer**

```
python hashmove.py /home/path/to/dir/a /home/path/to/dir/b -l
```

**verify against another hash or set of hashes**

```
python hashmove.py "/home/path to/dir/you question" /home/path/to/dir/with/hashes -v
```

**Install new packages in Python**
Windows: `S:\avlab\win_venv\Scripts\python -m pip install package_name`

Mac: `/Volumes/special/DeptShare/special/avlab/venv/bin/python3.11 -m pip install package_name`

#### credits / timeline

- Nov 2015 through Jan 2018 Originally written by Brenden Coates
- Aug 2018 through ?? Ian Lessing maintenance, refinements, additional functionality
- Jan through Aug 2023 Joh-Michael Kirchner maintenace, bug fixes, Python 3.11 compatibility



