Documentation for AV Lab procedures is maintained in UCSB Library Confluence Wiki at:

- https://ucsb-atlas.atlassian.net/l/cp/XasJxuZk (Henri Temianka Audio Preservation Lab)

Please see Confluence wiki for instructions on how to use the Python Microservices scripts to process digitized audio.


