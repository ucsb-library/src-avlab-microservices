#!/usr/bin/env python
'''
Make Derivatives for Web

walk the "repo" directory, select a source .WAV file preferring the "Broadcast" .WAV if available,
and create a audio level normalized MP3 file for any recording that doesn't already have an MP3 file.

'''
import os
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--source", required=True, help="The directory under which will be found .WAV files wich will be converted to MP3s")
parser.add_argument("--destination", help="The directory where the new MP3 files will be created.  If not specified the MP3 will be placed in the same directory as the source .WAV")
parser.add_argument("--normalizationScheme", help="The normalizaton scheme to be used.  Either 'ebu' or 'rms' ")
args = parser.parse_args()

rootDir = args.source

def ismp3(path):
    if path.endswith('.mp3'):
        return True
    else:
        return False


def mp3sInFileList(fileList):
    mp3files = []
    for fname in fileList:
        if(ismp3(fname)):
            mp3files.append(fname)
    return mp3files

def isNonMasterWav(path):
    if path.endswith('.wav') and not path.endswith('m.wav'):
        return True
    else:
        return False


def isBroadcastWav(fileList):
    broadcastWavExists = False
    for fname in fileList:
        if(fname.endswith('b.wav')): # there may be some false positives with this test
            broadcastWavExists = True
            broadcastWav = fname
    if(broadcastWavExists):
        return broadcastWav
    else:
        return False


def pickSourceWav(fileList):
    for fname in fileList:
        if(fname.endswith('.wav') and not fname.endswith('m.wav')):
            return fname


def makeMp3(dirName, sourceBroadcastWav):
    global args
    if dirName == rootDir:
        return
    destinationFileName, wav = os.path.splitext(sourceBroadcastWav)
    print("\ncreating a normalized MP3 file for: " + dirName + "/" + destinationFileName + "\n")

    print args
    # defalut normalization scheme is rms but you can specify one on the command line if you like
    if(args.normalizationScheme != None):
        print("using normalization scheme from command line")
        normalizationScheme = args.normalizationScheme
    else:
        print("using default rms normalization scheme")
        normalizationScheme = "rms"


## example command invocation:    python ffmpeg-normalize -o ~/Downloads/temp.mp3 --audio-codec libmp3lame  -nt ebu -v --target-level -16
    # here we call 'ffmpeg-normalize' with various flags so it will create the normalized MP3 file. LUFS -16
    subprocess.call(["python", "venv/bin/ffmpeg-normalize", dirName + "/" + sourceBroadcastWav, "-o", dirName + "/" + destinationFileName + '.mp3', "-nt", normalizationScheme, "--target-level", "-16", "--audio-codec", "libmp3lame", "--output-format", "mp3", "--audio-bitrate", "320k", "--true-peak", "-0.1"])


def main():
    '''
    main() function runs if this script is called directly by the python interpreter rather than being imported into some other Python script
    '''
    global conf
    for dirName, subdirList, fileList in os.walk(rootDir):
        if(any(ismp3(n) for n in fileList)):
            print("skipping " + dirName + "/ as it has at least one MP3 already" + "\n")
            mp3sInDir = mp3sInFileList(fileList)
        else:
            # print(dirName + " NEEDS an MP3 added")
            if(any(isBroadcastWav(n) for n in fileList)):
                broadcastWav = pickBroadcastWav(fileList)
                makeMp3(dirName, broadcastWav)
            elif(any(isNonMasterWav(n) for n in fileList)):
                ## no broadcast wav in dir to serve as source so we search for plain .wav
                sourceWav = pickSourceWav(fileList)
                makeMp3(dirName, sourceWav)
            else:
                print("skipping " + dirName + "/ as no .WAV  was found in this directory to use as a source." + "\n")

    print("\n")

if __name__ == '__main__':
    main()

